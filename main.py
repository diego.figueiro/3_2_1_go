from flask import Flask, render_template, request, session
import quiz_data
app = Flask(__name__)

@app.route('/')
def index():
    return render_template("index.html")


@app.route('/quiz1')
def quiz1():
    q = quiz_data.n1()
    return render_template("quiz_gabarito.html", 
    	comando   = q.comando, 
    	codigo    = q.codigo, 
    	respostas = q.respostas, 
    	script    = q.script, 
    	prox      = "'/quiz2'")

@app.route('/modelo')
def modelo():
	return render_template("quiz_modelo.html")