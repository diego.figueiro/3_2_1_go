# 3_2_1_Go

Projeto Integrado de 'Interação Humano Computador' e 'Paradigmas das Linguagens de Programação' - '3...2...1...Go!' Orientador: Aderbal Botelho ( https://br.linkedin.com/in/aderbalbotelho/pt-br )

<h1>Projeto - 3... 2... 1... Go!</h1> 

<p align="center">
   <img src="http://img.shields.io/static/v1?label=STATUS&message=EM%20DESENVOLVIMENTO&color=RED&style=for-the-badge"/>
</p>

> Status do Projeto: Desenvolvimento Conceitual :heavy_check_mark:

...

> Orientador: [Aderbal Botelho](https://br.linkedin.com/in/aderbalbotelho/pt-br)

...

### Tópicos 
:small_blue_diamond: [DOCUMENTAÇÃO](#documentação)

:small_blue_diamond: [Configurando o Git](#configurando-git-bash)

... 
### DOCUMENTAÇÃO
:raising_hand: [1 Participações Individuais](#1-participações-e-contribuições-individuais)

## 1 Participações e Contribuições Individuais
<p align="justify">

...

Cronograma:

> Debate em Grupo com a equipe para decisão inicial conceitual 17/03/2021

> Criando repositório no GitLab 19/03/2021

> Primeiro brainstorm de funções a serem implementadas 19/03/2021

>

...

Trabalho individual:

- 21907601 - Breno Sanchi Cardoso do Amaral:

...

- 21906410 - Diego Figueiró:

...

- 21900953 - Gabriel Vieira Neves:

...

- 21907417 - Shivaram Agnijan Palackpillil:

...

...

Informações adicionais sobre participações individuais podem ser consultadas verificando os commits realizados no gitlab.




</p>

...

## Configurando Git Bash 

<p align="justify">

  0)Se você deseja realmente entender o que está fazendo e deixar tudo configurado redondinho, acompanhe essa playlist com um tutorial bem explicado e seja um bom ultilizador iniciante de Git:
   https://www.youtube.com/watch?v=Jt4Z1vwtXT0&list=PLhW3qG5bs-L8YSnCiyQ-jD8XfHC2W1NL_
   
  1)Para trabalhar com o Projeto, primeiro instale o ''Git Bash''

   Faça o download do git em https://git-scm.com/ e instale.

  2)Clique com o botão direito e Abra o Git Bash, em seguida digite o comando:

   git --version

  3)Configure o Git Bash com seu nome de usuário do GitLab e o Email através dos seguintes comandos:

   git config --global user.name “nomedousuariogitlab”

   git config --global user.email “email@sempreceub.com”
   
  4)Verifique suas credenciais se estão corretas:
  
   git config --global --list

  5)Para clonar o projeto, digite:

   git clone "https://gitlab.com/diego.figueiro/3_2_1_go"

  6)Sempre antes de começar qualquer nova alteração ou inserção, use o comando para verificar se está tudo atualizado com o projeto em nuvem:

   git pull

  7)Faça suas inserções e alterações, e ao final verifique o status, adicione e verifique o status novamente:

   git status

   git add .

   git status

  8)Agora adicione uma mensagem curta que defina suas inserções e alterações, usando:

   git commit -m "mensagem aqui"

  9)Estamos quase prontos para enviar suas alterações, mas antes precisamos garantir que não ocorra erro entre versões das suas alterações e dos seus colegas de trabalho, esse passo é extremamente importante, use:

   git pull --rebase

  10)Agora sim, para enviar suas inserções e alterações para o projeto no GitLab, use:

   git push
 
</p>

... 

## Linguagens, e outras extensões :books:

- [Python](https://www.python.org/doc/)
- [GoLang](https://golang.org/doc/)
- [MySQL](https://dev.mysql.com/doc/refman/8.0/en/language-structure.html)

...

## Tarefas em aberto

Tópicos listados entre tarefas/funcionalidades que ainda precisam ser implementadas no site.

:memo: Iniciar main.py

## Desenvolvedores

Time responsável pelo desenvolvimento do projeto:

- RA: 21907601 [Breno Sanchi](https://gitlab.com/breno.amaral)
- RA: 21906410 [Diego Figueiró](https://gitlab.com/diego.figueiro)
- RA: 21900953 [Gabriel Neves](https://gitlab.com/gabriel.neves)
- RA: 21907417 [Shivaram Palackapillil](https://gitlab.com/shivaram.agnijan)

## Licença 

Direitos - [Não compramos licença]() (não copia ai namoral)

Copyright :copyright: 2021 - 3... 2... 1... Go!
