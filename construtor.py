from random import shuffle


class ConstrutorQuiz():
	'''	Classe que formatará os dados do quiz para o documento .html
		
	modelo de uso:
		
		quiz = ConstrutorQuiz(
		"""
		package main
		import "fmt"

		func main() {
			fmt.<<Println>>("Hello, world!")
		}"""
		,
		["print", "imprimir", "Print", "escrever"]
		)


	a partir disso, será possível acessar os atributos 
	"comando", "codigo", "respostas" e "script"
	'''

	
	def __init__(self, comando, codigo, alternativas_erradas):
		self.comando   = self.formatar_comando(comando)
		self.codigo    = self.formatar_codigo(codigo)
		self.respostas = self.formatar_respostas(codigo, alternativas_erradas)
		self.script    = self.formatar_script()

	
	def formatar_comando(self, comando):
		comando = comando.split("\n")
		for i in range(len(comando)):
			comando[i] = '<h1>' + comando[i] + '</h1>'

		return '\n'.join(comando)


	def formatar_codigo(self, codigo):
		num_cx_resposta = 0

		linhas = codigo.replace(" ", "&nbsp;")
		linhas = linhas.replace("\t", "&nbsp;&nbsp;&nbsp;&nbsp;")
		linhas = linhas.split("\n")
		
		for i in range(len(linhas)):
			linha = linhas[i]
			
			if "<<" in linha:
				linha = linha.replace("<<", "###<<")
				linha = linha.replace(">>", ">>###")

				itens_linha = linha.split("###")
				for x in range(len(itens_linha)):
					trecho_linha = itens_linha[x]

					if "<<" in trecho_linha:
						trecho_linha = '<div id="cx{}" class="caixa_resposta" ondrop="drop(event)" ondragover="allowDrop(event)"></div>'.format(num_cx_resposta)
						num_cx_resposta += 1
					else:
						trecho_linha = '<div class="codigo">' + trecho_linha + '</div>'

					itens_linha[x] = trecho_linha

				linha = '<div class="linha">' + ''.join(itens_linha) + '</div>'

			else:
				linha = '<div class="linha"><div class="codigo">' + linha + '</div></div>'

			linhas[i] = linha
			

		return '\n'.join(linhas)

	
	def formatar_respostas(self, codigo, alternativas_erradas):
		respostas_corretas  = []
		respostas_possiveis = alternativas_erradas

		while "<<" in codigo:
			abertura = codigo.index("<<")
			fechamento = codigo.index(">>")

			respostas_corretas.append( codigo[abertura+2:fechamento] )
			codigo = codigo[:abertura-1] + codigo[fechamento+2:]

		
		self.respostas_corretas = respostas_corretas
		respostas_possiveis.extend(respostas_corretas)
		shuffle(respostas_possiveis)
		self.respostas_possiveis = respostas_possiveis.copy()
		
		num_cx_opcao = 0
		for i in range(len(respostas_possiveis)):
			respostas_possiveis[i] = '''<div id="o{num}" class="caixa_opcoes" ondrop="drop(event)" ondragover="allowDrop(event)">
			<div id="{opcao}" class="opcoes" ondragstart="dragStart(event)" ondrag="dragging(event)" draggable="true">{opcao}</div>
			</div>'''.format(num=num_cx_opcao, opcao=respostas_possiveis[i])
			num_cx_opcao += 1

		return '\n'.join(respostas_possiveis)

	
	def formatar_script(self):
		num_cx = 0
		script = "var local_palavras = {"
		for i in range( len(self.respostas_possiveis) - 1 ):
			script += '{}: "o{}",'.format(self.respostas_possiveis[i], num_cx)
			num_cx += 1
		script += '{}: "o{}"'.format(self.respostas_possiveis[-1], num_cx)
		script += '}\n'
		
		num_cx = 0
		script += 'var resposta_correta = ['
		for i in range( len(self.respostas_corretas)-1 ):
			script += '["{}", "cx{}"],'.format(self.respostas_corretas[i], num_cx)
			num_cx += 1
		script += '["{}", "cx{}"]]\n'.format(self.respostas_corretas[-1], num_cx)

		return script
