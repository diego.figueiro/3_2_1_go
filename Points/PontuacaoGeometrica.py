def PontuacaoGeometrica():
    total, ponto, seq = 0, 50, 1.2
    for cont in range(0, 10):
        print("\nsua pontuação atual é", round(total, 1))
        print("pergunta valendo", round(ponto, 1), "pontos.")
        if ponto != 50:
            print("Bonus X"+str(round(ponto/50, 2)))
        log = str(input("resposta: "))
        if log == '':
            resposta = True
        else:
            resposta = False

        if resposta == True:
            total += ponto
            ponto = round(seq * ponto, 1)
            print("resposta CORRETA")
        else:
            ponto = 50
            print("resposta INCORRETA")

    print("\nsua pontuação final é", round(total, 1))
