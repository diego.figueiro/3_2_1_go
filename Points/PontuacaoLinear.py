def PontuacaoLinear():
    total, ponto, seq = 0, 50, 1
    for cont in range(0, 10):
        print("\nsua pontuação atual é", total)
        print("pergunta valendo", ponto, "pontos.")
        if ponto != 50:
            print("Bonus X"+str(round(ponto, 1)/50))
        log = str(input("resposta: "))
        if log == '':
            resposta = True
        else:
            resposta = False

        if resposta == True:
            total += ponto
            seq += 0.25
            ponto = round(seq * 50, 1)
            print("resposta CORRETA")
        else:
            ponto = 50
            seq = 1
            print("resposta INCORRETA")

    print("\nsua pontuação final é", total)

    PontuacaoLinear()
