from construtor import ConstrutorQuiz


def n1():
	return ConstrutorQuiz(
# comando do quiz
"""Faça com que seja impresso
'Hello, world!'"""
,
# codigo do quiz a ser completado
# a resposta correta fica entre '<<' e '>>', sendo removida pelo construtor
"""package main
import "fmt"

func main() {
	fmt.<<Println>>("Hello, world!")
}"""
,
# as outras alternativas de preenchimento do código (não é necessário colocar a resposta correta)
["print", "imprimir", "Print", "escrever"]
)


def n2():
	pass


def n3():
	pass